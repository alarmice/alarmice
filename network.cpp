//#include <QApplication>
#include <QDateTime>

#include "network.h"

Network::Network(QObject *parent) : QObject(parent)
{
}

Network::~Network()
{
}

QVariantList Network::urlMapList() const
{
    return m_urlMapList;
}

void Network::setUrlMapList(const QVariantList urlMapList)
{
    m_urlMapList = urlMapList;
}

void Network::exec()
{
    if(urlMapList().isEmpty())
    {
        qWarning() << "Network: blank URL list";
        emit error("Network: blank URL list");
        return;
    }
    m_urlsLeft = urlMapList().count();

    Settings *settings = new Settings(this);
    connect(settings, SIGNAL(error(QString)), this, SLOT(onError(QString)));
    settings->readAll();

    QTimer *timer = new QTimer();
    timer->setSingleShot(true);
    connect(timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    timer->start(settings->urlTimeout());

    delete settings;

    QNetworkAccessManager *nam = new QNetworkAccessManager(this);
    connect(nam, SIGNAL(finished(QNetworkReply *)), this, SLOT(onFinished(QNetworkReply *)));
    connect(nam, SIGNAL(networkAccessibleChanged(QNetworkAccessManager::NetworkAccessibility)),
            this, SLOT(onNetworkAccessibleChanged(QNetworkAccessManager::NetworkAccessibility)));

    foreach(QVariant var, urlMapList()) {
        QVariantMap map = var.toMap();
        QString urlStr = map.keys().at(0);
        QUrl url(urlStr, QUrl::StrictMode);
        if(!url.isValid())
        {
            qWarning() << "Network: invalid URL" << urlStr;
            if(--m_urlsLeft)
            {
                emit error("Networks: all URLs are invalid");
                return;
            }
            continue;
        }
        // отправить запрос по текущему URL
        QNetworkRequest req(url);
//        appendUrlResult(QString("REQUEST %1").arg(url.host()));
        QNetworkReply *rep = nam->get(req);
        connect(rep, SIGNAL(finished()), this, SLOT(onReplyFinished()));
        connect(rep, SIGNAL(downloadProgress(qint64,qint64)), timer, SLOT(start()));
    }
}

void Network::onTimeout()
{
    QNetworkAccessManager *nam = this->findChild<QNetworkAccessManager *>();
    QList<QNetworkReply *> replies = nam->findChildren<QNetworkReply *>();
    foreach (QNetworkReply * rep, replies) {
        appendUrlResult(QString("%1 - TIMEOUT").arg(rep->url().toDisplayString()));
        rep->deleteLater();
    }
    nam->deleteLater();
//    emit error("Network: timeout");

//    if(m_alias.isEmpty())
//    {
//        emit error("Network: timeout");
//    }
}

void Network::onFinished(QNetworkReply *rep)
{
}

void Network::onReplyFinished()
{
    QNetworkReply *rep = qobject_cast<QNetworkReply*>(sender());
    QNetworkAccessManager *nam = rep->manager();
    if(rep->error() == QNetworkReply::NoError && m_alias.isEmpty())
    {
        m_data = rep->readAll();
        m_alias = alias(rep->url());
        appendUrlResult(QString("%2 - %1").arg("OK").arg(rep->url().host()));
        rep->deleteLater();
        emit done(m_data, m_alias);
    }
    appendUrlResult(QString("%2 - %1").arg(rep->error()).arg(rep->url().host()));

    if(!--m_urlsLeft)
    {
        rep->deleteLater();
        nam->deleteLater();
//        if(m_alias.isEmpty())
//        {
//            emit error("Network: error downloading chart or data");
//        }
        emit closeSession();
    }
}

void Network::onReplyError(QNetworkReply::NetworkError code)
{
}

void Network::onNetworkAccessibleChanged(QNetworkAccessManager::NetworkAccessibility)
{
    emit error("Network: network inaccessible");
    emit closeSession();
}

void Network::appendUrlResult(QString result)
{
    QDateTime now = QDateTime::currentDateTime();
    QString nowStr = now.toString("hh:mm:ss.") + QString("%1").arg(now.time().msec());
    m_urlResult.append(QString("%1 : %2").arg(nowStr).arg(result));
}

QString Network::alias(QUrl url)
{
    QString aliasStr;
    foreach(QVariant var, urlMapList()) {
        QVariantMap map = var.toMap();
        if(map.keys().at(0).indexOf(url.host()) != -1)
        {
            aliasStr = map.values().at(0).toString();
        }
    }
    return aliasStr;
}

void Network::onError(QString errorString)
{
}
