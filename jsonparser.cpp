#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QStringList>

#include "jsonparser.h"

JsonParser::JsonParser(QObject *parent) :
    QObject(parent)
{
}

void JsonParser::validate(QByteArray data)
{
    QJsonParseError jsonError;
    QJsonDocument json = QJsonDocument::fromJson(data, &jsonError);
    if(json.isNull())
    {
        emit error("Parser: " + jsonError.errorString());
        return;
    }
    if(json.isObject())
    {
        QJsonObject root = json.object();
        QVariantMap map = root.toVariantMap();
        emit done(map);
    }
}
