#include <QDesktopWidget>
#include <QGridLayout>
#include <QListWidget>
#include <QFontMetrics>
#include <QTime>
#include <QStringList>
#include <QMargins>
#include <QFontMetrics>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_starting = true;
    m_isCloseDemand = false;
    if(QSystemTrayIcon::isSystemTrayAvailable())
    {
        QSystemTrayIcon *tray = new QSystemTrayIcon(QIcon("://images/alarmice2.png"), this);
        connect(tray, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
                this, SLOT(showHide(QSystemTrayIcon::ActivationReason)));
        tray->setToolTip(QString("%1").arg(PRODUCT_NAME));
        QMenu *menu = new QMenu(this);
        QAction *closeAction = menu->addAction("Завершить");
        connect(closeAction, SIGNAL(triggered()), this, SLOT(closeByDemand()));
        tray->setContextMenu(menu);
        tray->show();
    }

    this->move(QPoint(0,0));
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(refreshData()), Qt::QueuedConnection);
    connect(this, SIGNAL(error(QString)), this, SLOT(onError(QString)));
    this->statusBar()->addPermanentWidget(&m_lastUpdatedLabel);

    QLabel *panelIdLabel = new QLabel();
    panelIdLabel->setObjectName("panelIdLabel");

    this->statusBar()->addPermanentWidget(panelIdLabel);
    this->statusBar()->addPermanentWidget(new QLabel(QString("v.%1").arg(VERSION_NUMBER)));

    QHBoxLayout *groupsLay = new QHBoxLayout(ui->groupsBox);
    ui->groupsBox->setLayout(groupsLay);

    m_logBoxTitle = ui->logBox->title();
    connect(ui->groupsBox, SIGNAL(clicked()), this, SLOT(resetLogFilter()));

    ui->pbResetFilter->setEnabled(false);

    addToLog(QString("%1 started").arg(PRODUCT_NAME));
    ui->actionLog->setChecked(false);
    ui->logBox->setHidden(true);
    ui->actionSettings->setChecked(true);
    m_age = 0;
    m_debugLevel = 0;
    m_savedLogRow = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onError(QString errorString)
{
    addToLog(errorString);
    m_lastUpdatedLabel.setText(QString("<font color='red'>%1</font>")
                               .arg(m_lastUpdatedStr));
    showLastStates();
    if(m_starting)
    {
        ui->actionLog->setChecked(true);
    }
    this->statusBar()->clearMessage();
}

void MainWindow::refreshData()
{
    qDebug() << QTime::currentTime() << Q_FUNC_INFO;
    Settings *settings = new Settings(this);
    connect(settings, SIGNAL(error(QString)), this, SLOT(setSettingsError(QString)));
    resetSettingsError();
    settings->readAll();
    QLabel *panelIdLabel = this->statusBar()->findChild<QLabel *>("panelIdLabel");
    panelIdLabel->setText(settings->panelId());
    m_timer.setInterval(settings->dataRefreshPeriod());
    if(!m_timer.isActive())
    {
        m_timer.start();
    }
    m_maxLogSize = settings->maxLogSize();
    m_dataTTL = settings->dataTTL();
    m_panelId = settings->panelId();
    settings->deleteLater();
    if(!settingsError())
    {
        this->statusBar()->showMessage("Downloading...");
        Chart *chart = this->findChild<Chart *>();
        if(chart)
        {
            chart->deleteLater();
        }
        chart = new Chart(this);
        connect(chart, SIGNAL(groupsChanged(Groups, QString)), this, SLOT(showGroups(Groups, QString)));
        connect(chart, SIGNAL(error(QString)), this, SLOT(onError(QString)));
        connect(chart, SIGNAL(urlResult(QString, QStringList)),
                this, SLOT(onUrlResult(QString, QStringList)));
        chart->readAll();
        this->setUpdatesEnabled(true);
    }
}

void MainWindow::showGroups(Groups groups, QString ftpAlias)
{
    this->setUpdatesEnabled(false);
    QList<QListWidget *> groupList = ui->groupsBox->findChildren<QListWidget *>();

    foreach (QListWidget *group, groupList) {
        delete group;
    }

    QStringList msgStrings;
    m_dataAge = 0;
    m_maxState = 0;

    // сформировать группы
    int maxHeight = 0;
    QLayout *layout = ui->groupsBox->layout();
    QMargins margins = layout->contentsMargins();
    QSystemTrayIcon *tray = this->findChild<QSystemTrayIcon *>();
    foreach (QList<NodeItem *> nodes, groups) {
        QListWidget *nodesListWidget = new QListWidget(ui->groupsBox);
        connect(nodesListWidget, SIGNAL(itemClicked(QListWidgetItem*)),
                this, SLOT(setLogFilter(QListWidgetItem*)));
        ui->groupsBox->layout()->addWidget(nodesListWidget);
        QListWidgetItem *header = new QListWidgetItem(groups.key(nodes));
        header->setBackground(QBrush(QColor(QRgb(0xFFF0F0F0))));
        header->setFont(QFont(nodesListWidget->font().family(),
                              nodesListWidget->font().pointSize(),
                              QFont::Bold));
        header->setTextAlignment(Qt::AlignHCenter);
        nodesListWidget->addItem(header);

        // сформировать узлы для текущей группы
        foreach (NodeItem *node, nodes) {
            nodesListWidget->addItem(node);
            m_maxState = qMax(node->state(), m_maxState);
            QString stateStr;
            switch (node->state()) {
            case 0: stateStr = "N/A"; break;
            case 1: stateStr = "READY"; break;
            case 2: stateStr = "MINOR"; break;
            case 3: stateStr = "MAJOR"; break;
            case 4: stateStr = "ALARM"; break;
            default: stateStr = "???";
            }
            QString amountStr;
            switch (node->amount()) {
            case 4: amountStr = "NN"; break;
            case 3: amountStr = "VN"; break;
            case 2: amountStr = "CN"; break;
            default: amountStr = "??";
            }

            m_dataAge = qMax(m_dataAge, node->modified().msecsTo(QDateTime::currentDateTime()) / 1000);
            node->setToolTip(QString("%2 id = %1, amount = %3")
                             .arg(node->id())
                             .arg(node->modified().toString("hh:mm"))
                             .arg(amountStr));

            // записать в журнал изменение состояния узла
            if(m_prevStates.value(node->id()) != node->state()
                    && !m_starting)
            {
                addToLog(QString("%1 - %3")
                         .arg(node->text())
                         //                         .arg(node->data(Qt::UserRole).toString())
                         .arg(stateStr));

                // включить узел во всплывающее сообщение
                bool isMessageNeed = false;
                if(node->state() >= 3 && node->amount() == 2) // п. 4.4.3 ТЗ
                {
                    isMessageNeed = true;
                } else if(node->state() == 4 && node->amount() == 3) // п. 4.4.2 ТЗ
                {
                    isMessageNeed = true;
                } else if((node->state() == 0 && ui->cbNa->isChecked())
                          || (node->state() == 1 && ui->cbReady->isChecked())
                          || (node->state() == 2 && ui->cbMinor->isChecked())
                          || (node->state() == 3 && ui->cbMajor->isChecked())
                          || (node->state() == 4 && ui->cbAlarm->isChecked())
                          )
                {
                    isMessageNeed = true;
                }

                if(isMessageNeed)
                {
                    msgStrings.append(node->text() + " - " + stateStr);
                }
            }
            m_prevStates.insert(node->id(), node->state());

        } // узлы текущей группы сформированы

        if(tray)
        {
            tray->setIcon(QIcon("://images/alarmice2.png"));

            if(!msgStrings.isEmpty())
            {
                tray->showMessage(QString("%1 (%2)").arg(PRODUCT_NAME).arg(m_panelId),
                                  msgStrings.join('\n'), QSystemTrayIcon::Warning, 1000);
            }
        }

        int listWidth = nodesListWidget->sizeHintForColumn(0);
        listWidth += margins.left();
        nodesListWidget->setMinimumWidth(listWidth);

        int rowsCount = nodesListWidget->count();
        QFontMetrics metrics(this->font());
        int itemsHeight = (nodesListWidget->sizeHintForRow(0) + metrics.xHeight())
                * (rowsCount + 1) - metrics.lineSpacing();
        maxHeight = qMax(maxHeight, itemsHeight);
    } // группы сформированы

    if(tray)
    {
        switch (m_maxState) {
        case 4:
            tray->setIcon(QIcon("://images/alarmiceA.png"));
            break;
        case 3:
            tray->setIcon(QIcon("://images/alarmiceM.png"));
            break;
        case 2:
            tray->setIcon(QIcon("://images/alarmiceMI.png"));
            break;
        default:
            tray->setIcon(QIcon("://images/alarmice2.png"));
            break;
        }
    }

//    if(m_starting)
//    {
//        m_dataAge = 0;
//    }
    qDebug() << m_dataAge << m_dataTTL;
    QDateTime now = QDateTime::currentDateTime();
    if(m_dataAge > m_dataTTL && tray && !m_starting)
    {
//        tray->showMessage(QString("%1 (%2)").arg(PRODUCT_NAME).arg(m_panelId), QString("Данные устарели!\n%1")
//                          .arg(now.toString("hh:mm:ss")),
//                          QSystemTrayIcon::Warning, 1000);
        emit error("The data is obsolete");
    } else {
        m_age = 0;
        QDateTime lastDataDT = now.addSecs(-m_dataAge);
        m_lastUpdatedStr = QString("%1 : %2").arg(ftpAlias)
                .arg(lastDataDT.toString("hh:mm"));
        m_lastUpdatedLabel.setText(QString("<font color='black'>%1</font>").arg(m_lastUpdatedStr));
    }

    maxHeight += margins.top() + margins.bottom();
    ui->groupsBox->setMinimumHeight(maxHeight);

    m_starting = false;
    this->statusBar()->clearMessage();
    this->setUpdatesEnabled(true);
}


void MainWindow::on_actionLog_toggled(bool checked)
{
    if(checked)
    {
        ui->logList->scrollToBottom();
        ui->logBox->show();
    } else {
        ui->logBox->hide();
    }
}

void MainWindow::setSettingsError(QString errorString)
{
    m_timer.stop();
    m_settingsError = true;
    addToLog(errorString);
    addToLog("The program is stopped, check the settings and restart");
    ui->actionLog->setChecked(true);
}

void MainWindow::addToLog(const QString &logStr, int level)
{
    // при необходимости удалить самую старую запись
    if(ui->logList->count() >= m_maxLogSize && m_maxLogSize != 0)
    {
        ui->logList->removeItemWidget(ui->logList->takeItem(0));
    }

    // сформировать строку и задать уровень сообщения
    QString logStrDt = QString("%1: %2")
            .arg(QDateTime::currentDateTime().toString(Qt::ISODate))
            .arg(logStr);
    QListWidgetItem *item = new QListWidgetItem(logStrDt);
    item->setData(Qt::UserRole, level);

    // записать сообщение в журнал
    ui->logList->addItem(item);

    if(level > m_debugLevel)
    {
        item->setHidden(true);
    }

    // если есть активный фильтр, применить его
    if(!m_logFilter.isEmpty())
    {
        setLogFilter(m_logFilter);
    }

    if(!ui->logList->hasFocus())
    {
        ui->logList->scrollToBottom();
    }
}

void MainWindow::on_actionSettings_toggled(bool checked)
{
    if(checked)
    {
        m_lastSize = this->size();
        ui->settingsBox->show();
    } else {
        ui->settingsBox->hide();
        this->resize(m_lastSize);
        qDebug() << "resize" << m_lastSize;
    }
}

void MainWindow::resetLogFilter()
{
    this->setUpdatesEnabled(false);
    m_logFilter.clear();
    QListWidgetItem *currentItem = ui->logList->currentItem();
    ui->logBox->setTitle(m_logBoxTitle);
    for(int row = 0; row < ui->logList->count(); row++) {
        QListWidgetItem *logItem = ui->logList->item(row);
        if(logItem->data(Qt::UserRole).toDouble() <= m_debugLevel)
        {
            logItem->setHidden(false);
        } else {
            logItem->setHidden(true);
        }
    }

    if(currentItem)
    {
        ui->logList->setCurrentItem(currentItem);
    }
    ui->pbResetFilter->setEnabled(false);
    ui->logList->scrollToBottom();
    this->setUpdatesEnabled(true);
}

void MainWindow::setLogFilter(QStringList items)
{
    this->setUpdatesEnabled(false);

    for(int row = 0; row < ui->logList->count(); row++) {
        bool isFound = false;
        QListWidgetItem *logItem = ui->logList->item(row);
        QString logStr = ui->logList->item(row)->text();

        foreach (QString item, items) {
            if(logStr.indexOf(item) != -1)
            {
                isFound = true;
                break;
            }
        }

        if(isFound)
        {
            logItem->setHidden(false);
        } else {
            logItem->setHidden(true);
        }
    }
    ui->pbResetFilter->setEnabled(true);
    ui->logList->scrollToBottom();
    this->setUpdatesEnabled(true);
}

void MainWindow::setLogFilter(QListWidgetItem *item)
{
    this->setUpdatesEnabled(false);

    m_logFilter.clear();
    ui->logBox->setTitle(QString("%1 (%2)").arg(m_logBoxTitle).arg(item->text()));

    if(item->icon().isNull()) // фильтр по группе
    {
        for(int row = 0; row < item->listWidget()->count(); ++row)
        {
            QListWidgetItem *logItem = item->listWidget()->item(row);
            m_logFilter.append(logItem->text());
        }
    } else { // фильтр по узлу
        m_logFilter.append(item->text());
    }
    setLogFilter(m_logFilter);
    ui->actionLog->setChecked(true);
    this->setUpdatesEnabled(true);
}

void MainWindow::refreshLog()
{

}

void MainWindow::on_pbResetFilter_clicked()
{
    ui->textFilter->clear();
    resetLogFilter();
}

void MainWindow::onUrlResult(QString senderName, QStringList result)
{
    addToLog(QString("%1 session log:").arg(senderName), 1);
    foreach(QString str, result)
    {
        addToLog(str, 1);
    }
}

void MainWindow::showLastStates()
{
    const int statesCount = 4;
    if(++m_age > statesCount)
    {
        m_age = statesCount;
        QSystemTrayIcon *tray = this->findChild<QSystemTrayIcon *>();
        if(tray)
        {
            tray->setIcon(QIcon("://images/aalarmice.png"));
            tray->showMessage(QString("%1 (%2)").arg(PRODUCT_NAME).arg(m_panelId), "Ошибка загрузки данных", QSystemTrayIcon::Warning, 1000);
        }
    }
    QList<QListWidget *> lists = ui->groupsBox->findChildren<QListWidget *>();
    foreach (QListWidget *list, lists) {
        for(int i = 1; i < list->count(); i++)
        {
            NodeItem *node = (NodeItem *)list->item(i);
            QIcon icon = node->icon();
            if(node->state() == 4)
                node->setIcon(QIcon(QString("://images/status_alarm%1.bmp").arg(statesCount - m_age)));

            if(node->state() == 3)
                node->setIcon(QIcon(QString("://images/status_major%1.bmp").arg(statesCount - m_age)));

            if(node->state() == 2)
                node->setIcon(QIcon(QString("://images/status_minor%1.bmp").arg(statesCount - m_age)));

            if(node->state() == 1)
                node->setIcon(QIcon(QString("://images/status_ready%1.bmp").arg(statesCount - m_age)));
        }
    }
}

void MainWindow::on_cbDebugLog_clicked(bool checked)
{
    if(checked)
    {
        m_debugLevel = 1;
    } else {
        m_debugLevel = 0;
    }
    resetLogFilter();
}

void MainWindow::on_textFilter_textEdited(const QString &arg1)
{
    this->setUpdatesEnabled(false);
    if(arg1.isEmpty())
    {
        ui->logBox->setTitle(m_logBoxTitle);
        ui->pbResetFilter->setEnabled(false);
        resetLogFilter();
    } else {
        ui->pbResetFilter->setEnabled(true);
        for(int row = 0; row < ui->logList->count(); row++) {
            if(ui->logList->item(row)->text().indexOf(arg1) != -1
                    && ui->logList->item(row)->data(Qt::UserRole).toDouble() <= m_debugLevel)
            {
                ui->logList->item(row)->setHidden(false);
            } else {
                ui->logList->item(row)->setHidden(true);
            }
        }
        ui->logBox->setTitle(QString("%1 (%2)").arg(m_logBoxTitle).arg(arg1));
    }
    this->setUpdatesEnabled(true);
}
