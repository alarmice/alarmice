#ifndef JSONPARSER_H
#define JSONPARSER_H

#include <QObject>
#include <QVariantMap>

class JsonParser : public QObject
{
    Q_OBJECT
public:
    explicit JsonParser(QObject *parent = 0);
signals:
    void error(QString errorStr);
    void done(QVariantMap map);
public slots:
    void validate(QByteArray data);
};

#endif // JSONPARSER_H
