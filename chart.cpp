#include <QDebug>
#include <QRegExp>
#include <QResizeEvent>


#include "chart.h"

Chart::Chart(QObject *parent) :
    QObject(parent)
{
}

void Chart::readAll()
{
    Settings *settings = new Settings();
    connect(settings, SIGNAL(error(QString)), this, SLOT(onError(QString)));
    settings->readAll();

    Data *data = new Data(this);
    connect(this, SIGNAL(chartChanged(QVariantMap)), data, SLOT(readAll()));
    connect(data, SIGNAL(dataChanged(QStringList, QString)),
            this, SLOT(setGroups(QStringList, QString)));
    connect(data, SIGNAL(error(QString)), this, SLOT(onError(QString)));
    connect(data, SIGNAL(urlResult(QString, QStringList)),
            this, SIGNAL(urlResult(QString, QStringList)));

    Network *net = new Network(this);
    connect(net, SIGNAL(closeSession()), net, SLOT(deleteLater()));
    connect(net, SIGNAL(done(QByteArray, QString)),
            this, SLOT(onDownload(QByteArray, QString)));
    connect(net, SIGNAL(error(QString)),
            this, SLOT(onError(QString)));
    QVariantList tempUrlMapList(settings->urlMapList());
    for(QVariantList::iterator i = tempUrlMapList.begin(); i != tempUrlMapList.end(); ++i) {
        QVariantMap map;
        QString key = QString("%1/%2.cht.json").arg((*i).toMap().keys().at(0)).arg(settings->panelId());
        QString alias = (*i).toMap().values().at(0).toString();
        map.insert(key, alias);
        *i = map;
    }
    net->setUrlMapList(tempUrlMapList);
    net->exec();

    delete settings;
}

void Chart::onDownload(QByteArray chart, QString alias)
{
    Network *net = qobject_cast<Network*>(sender());
    emit urlResult("Chart", net->urlResult());
    m_ftpAlias = alias;
    JsonParser *parser = new JsonParser(this);
    connect(parser, SIGNAL(done(QVariantMap)),
            this, SLOT(setChart(QVariantMap)));
    connect(parser, SIGNAL(error(QString)),
            this, SLOT(onError(QString)));
    parser->validate(chart);
}

void Chart::setChart(QVariantMap arg)
{
    setVersion(arg.value("version").toString());
    if(checkVersion())
    {
        if(!arg.isEmpty())
        {
            m_chart = arg;
            emit chartChanged(arg);
        } else {
            qWarning() << "Chart: no data";
        }
    } else {
        emit error("Chart: incorrect version");
    }

    JsonParser *parser = this->findChild<JsonParser *>();
    if(parser)
    {
        parser->deleteLater();
    }
}

void Chart::setGroups(QStringList data, QString ftpAlias)
{
    m_groups.clear();
    // получить список групп
    QVariantList groupList = m_chart.value("groups").toList();

    // для каждой группы
    foreach (QVariant group, groupList) {
        // получить имя группы
        QString groupName = group.toMap().value("name").toString();

        // получить список узлов группы
        QVariantList nodesList = group.toMap().value("nodes").toList();
        QList<NodeItem *> groupItems;
        QVariantList::iterator i;

        // для каждого узла
        for(i = nodesList.begin(); i != nodesList.end(); ++i) {
            // получить данные узла
            QVariantMap nodeParams = (*i).toMap();

            // создать объект Node и установить начальные значения
            NodeItem *node = new NodeItem();
            node->setName(nodeParams.value("name").toString());
            node->setId(nodeParams.value("id").toString());

            // установить свойства объекта-узла по данным схемы
            QString amountStr = nodeParams.value("amount").toString().trimmed();
            if(amountStr == "NN")
            { node->setAmount(4); }
            else if(amountStr == "VN")
            { node->setAmount(3); }
            else if(amountStr == "CN")
            { node->setAmount(2); }
            else {
                emit error(QString("Chart: invalid amount in %1.%2")
                           .arg(groupName)
                           .arg(node->name()));
                continue;
            }

            // определить текущее состояние узла
            setNodeData(node, data);

            // добавить узел в список
            groupItems.append(node);
        }
        m_groups.insert(groupName, groupItems);
    }
    emit groupsChanged(m_groups, ftpAlias);
}

void Chart::setNodeData(NodeItem* node, QStringList data)
{
    QStringList errors;
    foreach (QString dataStr, data) {
        QStringList dataList = dataStr.split(':');
        if(dataList.count() == 3)
        {
            if(dataList.at(1) == node->id())
            {
                QDateTime dt;
                dt.setTime_t(dataList.at(0).toInt());

                bool ok;
                int state = dataList.at(2).toInt(&ok);
                if(ok && dt.isValid() && state >= 0 && state <= 4)
                {
                    node->setModified(dt);
                    if(state == 0)
                    {
                        node->setState(0);
                        node->setIcon(QIcon("://images/status_na.bmp"));
                    } else if(state == 1)
                    {
                        node->setState(1);
                        node->setIcon(QIcon("://images/status_ready.bmp"));
                    } else if(state == 2)
                    {
                        node->setState(2);
                        node->setIcon(QIcon("://images/status_minor.bmp"));
                    } else if(state == 3)
                    {
                        node->setState(3);
                        node->setIcon(QIcon("://images/status_major.bmp"));
                    } else if(state == 4)
                    {
                        node->setState(4);
                        node->setIcon(QIcon("://images/status_alarm.bmp"));
                    }
                } else {
                    node->setState(0);
                    node->setIcon(QIcon("://images/status_na.bmp"));
                    errors.append(QString("Chart: Invalid data: %1").arg(dataStr));
                }
            }
        }
    }
    if(!errors.isEmpty())
    {
        emit error(errors.join('\n'));
    }
}

void Chart::onError(QString errorString)
{
    qWarning() << "Chart:" << errorString;
    emit error("Chart: " + errorString);
}


bool Chart::checkVersion()
{
    QRegExp pattern(QString("^%1").arg(version()));
    if(!pattern.indexIn(QString(VERSION_NUMBER)) == 0)
    {
        return false;
    }
    return true;
}

