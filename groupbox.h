#ifndef GROUPBOX_H
#define GROUPBOX_H

#include <QGroupBox>
#include <QList>
#include <QVariantMap>

class GroupBox : public QGroupBox
{
    Q_OBJECT
    Q_PROPERTY(QList<QVariantMap> nodes READ nodes WRITE setNodes NOTIFY nodesChanged)
public:
    explicit GroupBox(QWidget *parent = 0);
    GroupBox(QString &name, QWidget *parent = 0)
    {
        QWidget::setParent(parent);
        setTitle(name);
    }
    
    QList<QVariantMap> nodes() const
    {
        return m_nodes;
    }

signals:

    void nodesChanged(QList<QVariantMap> arg);

public slots:

    void setNodes(QList<QVariantMap> arg);

private:

    QList<QVariantMap> m_nodes;
};

#endif // GROUPBOX_H
