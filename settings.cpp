#include <QDebug>
#include <QFile>
#include <QApplication>

#include "settings.h"

Settings::Settings(QObject *parent) :
    QObject(parent)
{
    JsonParser *parser = new JsonParser(this);
    connect(this, SIGNAL(validateJson(QByteArray)),
            parser, SLOT(validate(QByteArray)));
    connect(parser, SIGNAL(error(QString)), this, SLOT(onError(QString)));
    connect(parser, SIGNAL(done(QVariantMap)), this, SLOT(validate(QVariantMap)));

}

void Settings::readAll()
{
    QString filePath = QApplication::applicationDirPath();
    QString fileName = QString("%1/%2.json")
            .arg(filePath)
            .arg(QString(PRODUCT_NAME).toLower());
    QFile f(fileName);
    if(f.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        data = f.readAll();
        f.close();
    } else {
        emit error(QString("Settings: %1: %2").arg(fileName).arg(f.errorString()));
    }
    emit validateJson(data);
}

void Settings::onError(QString errorString)
{
    qWarning() << "Settings:" << errorString;
    emit error(errorString);
}

void Settings::validate(QVariantMap map)
{
    setVersion(map.value("version").toString());
    setPanelId(map.value("panelId").toString());
    setDataRefreshPeriod(map.value("dataRefreshPeriod").toDouble());
    setUrlTimeout(map.value("urlTimeout").toDouble());
    QVariantList urlMapList = map.value("urlList").toList();
    setUrlMapList(urlMapList);
    setMaxLogSize(map.value("maxLogSize").toDouble());
    setDataTTL(map.value("dataTTL").toDouble());
}

