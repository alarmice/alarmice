#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QGridLayout>
#include <QTimer>
#include <QSystemTrayIcon>
#include <QCloseEvent>
#include <QLabel>
#include <QIcon>
#include <QListWidget>
#include <QListWidgetItem>

#include "settings.h"
#include "chart.h"
#include "groupbox.h"
#include "nodeitem.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void addToLog(const QString &, int level = 0);

signals:
    void error(QString errorString);
public slots:
    void onError(QString errorString);
    void refreshData();

    void showHide(QSystemTrayIcon::ActivationReason reason)
    {
        if(reason == QSystemTrayIcon::Trigger)
        {
            if(this->isVisible())
            {
                this->hide();
            } else {
                this->show();
                this->activateWindow();
            }
        }
    }

    void closeByDemand()
    {
        m_isCloseDemand = true;
        this->close();
    }
    void setSettingsError(QString errorString);
private slots:
    void showGroups(Groups, QString);
    void on_actionLog_toggled(bool);
    void resetSettingsError() { m_settingsError = false; }
    void on_actionSettings_toggled(bool);
    void resetLogFilter();
    void setLogFilter(QStringList aliases);
    void setLogFilter(QListWidgetItem *item);
    void refreshLog();
    void on_pbResetFilter_clicked();
    void onUrlResult(QString senderName, QStringList result);

    void on_cbDebugLog_clicked(bool checked);

    void on_textFilter_textEdited(const QString &arg1);

private:
    Ui::MainWindow *ui;
    bool m_isCloseDemand;
    QLabel m_lastUpdatedLabel;
    QString m_lastUpdatedStr;
    qint64 m_dataAge;
    qint64 m_dataTTL;
    bool m_starting;
    QTimer m_timer;
    bool m_settingsError;
    bool settingsError() {return m_settingsError;}
    QStringList m_logFilter;
    QString m_logBoxTitle;
    int m_errorsCount;
    void showLastStates();
    int m_age;
    QMap<QString, int> m_prevStates;
    QMap<QString, bool> m_prevChecked;
    int m_maxState;
    QSize m_lastSize;
    int m_maxLogSize;
    int m_debugLevel;
    int m_savedLogRow;
    QString m_panelId;
protected:
    void closeEvent(QCloseEvent *event)
    {
        if (m_isCloseDemand) {
            event->accept();
        } else {
            this->hide();
            event->ignore();
        }
    }
};

#endif // MAINWINDOW_H
