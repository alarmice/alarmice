#ifndef NODEITEM_H
#define NODEITEM_H

#include <QListWidgetItem>
#include <QDateTime>

class NodeItem : public QListWidgetItem
{
//    Q_ENUMS(Amount State)
public:
    explicit NodeItem() {setState(0); setAmount(4);}
//    enum Amount{CN = 2, VN = 3, NN = 4};
//    enum State{NA = 0, READY = 1, MINOR = 2, MAJOR = 3, ALARM = 4};

    QString id() const {return data(Qt::UserRole).toString();}
    QString name() const {return text();}
    int amount() const {return data(Qt::UserRole + 1).toInt();}
    int state() const {return data(Qt::UserRole + 2).toInt();}
    QDateTime modified() const {return data(Qt::UserRole + 3).toDateTime();}

    void setId(QString arg) {setData(Qt::UserRole, arg);}
    void setName(QString arg) {setText(arg);}
    void setAmount(int arg) {setData(Qt::UserRole + 1, arg);}
    void setState(int arg) {setData(Qt::UserRole + 2, arg);}
    void setModified(QDateTime arg) {setData(Qt::UserRole + 3, arg);}
};

//Q_DECLARE_METATYPE(NodeItem::Amount)
//Q_DECLARE_METATYPE(NodeItem::State)

#endif // NODEITEM_H
