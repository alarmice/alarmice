#include <QDebug>

#include "groupbox.h"

GroupBox::GroupBox(QWidget *parent) :
    QGroupBox(parent)
{
}

void GroupBox::setNodes(QList<QVariantMap> arg)
{
    m_nodes = arg;
    emit nodesChanged(arg);
}
