#ifndef CHART_H
#define CHART_H

#include <QObject>
#include <QVariantMap>
#include <QStringList>
#include <QDebug>

#include "nodeitem.h"
#include "network.h"
#include "settings.h"
#include "jsonparser.h"
#include "data.h"

typedef QMap<QString, QList<NodeItem *> > Groups;

class Chart : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString version READ version WRITE setVersion NOTIFY versionChanged)
    Q_PROPERTY(QVariantMap chart READ chart WRITE setChart NOTIFY chartChanged)
public:
    explicit Chart(QObject *parent = 0);
    QVariantMap chart() const { return m_chart; }
    QString version() const { return m_version; }
    Groups groups() const { return m_groups; }

signals:
    void error(QString errorString);
    void versionChanged(QString arg);
    void chartChanged(QVariantMap arg);
    void dataChanged(QStringList arg);
    void groupsChanged(Groups arg, QString ftpAlias);
    void urlResult(QString, QStringList);

public slots:
    void readAll();
    void onDownload(QByteArray chart, QString alias);
    void setVersion(QString arg) { m_version = arg; emit versionChanged(arg); }
    void setChart(QVariantMap arg);
    void setGroups(QStringList data, QString ftpAlias);
    void setNodeData(NodeItem *, QStringList);
    void onError(QString errorString);

private slots:

private:
    QVariantMap m_chart;
    QString m_version;
    bool checkVersion();
    Groups m_groups;
    QString m_ftpAlias;
};

#endif // CHART_H
