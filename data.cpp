#include <QThread>

#include "data.h"

Data::Data(QObject *parent) :
    QObject(parent)
{
}

void Data::readAll()
{
    Settings *settings = new Settings();
    connect(settings, SIGNAL(error(QString)), this, SLOT(onError(QString)));
    settings->readAll();

    Network *net = new Network(this);
    connect(net, SIGNAL(closeSession()), net, SLOT(deleteLater()));
    connect(net, SIGNAL(done(QByteArray, QString)),
            this, SLOT(onDownload(QByteArray, QString)));
    connect(net, SIGNAL(error(QString)), this, SLOT(onError(QString)));

    QVariantList tempUrlMapList(settings->urlMapList());
    for(QVariantList::iterator i = tempUrlMapList.begin(); i != tempUrlMapList.end(); ++i) {
        QVariantMap map;
        QString key = QString("%1/%2.dat").arg((*i).toMap().keys().at(0))
                .arg(QString(PRODUCT_NAME).toLower());
        QString alias = (*i).toMap().values().at(0).toString();
        map.insert(key, alias);
        *i = map;
    }
    net->setUrlMapList(tempUrlMapList);
    net->exec();
    delete settings;
}

void Data::setData(QStringList arg)
{
    foreach (QString str, arg) {

        int fieldsCount = str.split(':').length();
        if (!str.trimmed().isEmpty() && fieldsCount != 3) {
            qWarning() << "Data: invalid format";
            emit error("Data: invalid format");
            return;
        }
    }
    m_data = arg;
    emit dataChanged(arg, m_ftpAlias);
}

void Data::onDownload(QByteArray data, QString alias)
{
    Network *net = qobject_cast<Network*>(sender());
    emit urlResult("Data", net->urlResult());
    m_ftpAlias = alias;
    setData(QString(data).split('\n'));
}

void Data::onError(QString errorString)
{
    qWarning() << "Data: " + errorString;
    emit error(errorString);
}


