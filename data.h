#ifndef DATA_H
#define DATA_H

#include <QObject>
#include <QStringList>
#include <QDebug>
#include <QDateTime>

#include "network.h"
#include "settings.h"


class Data : public QObject
{
    Q_OBJECT

public:
    explicit Data(QObject *parent = 0);
    Q_PROPERTY(QStringList data READ data WRITE setData NOTIFY dataChanged)
    QStringList data() const
    {
        return m_data;
    }

signals:
    void error(QString errorString);
    void dataChanged(QStringList arg, QString alias);
    void urlResult(QString, QStringList);

public slots:
    void readAll();
    void setData(QStringList arg);

private slots:
    void onDownload(QByteArray data, QString alias);
    void onError(QString errorString);
private:
    QStringList m_data;
    QString m_ftpAlias;
};

#endif // DATA_H
