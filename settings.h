#ifndef SETTINGS_H
#define SETTINGS_H

#include <QVariantMap>
#include <QObject>
#include <QStringList>
#include <QUrl>
#include <QDebug>

#include "jsonparser.h"

class Settings : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString version READ version WRITE setVersion NOTIFY versionChanged)
    Q_PROPERTY(QString panelId READ panelId WRITE setPanelId NOTIFY panelIdChanged)
    Q_PROPERTY(int dataRefreshPeriod READ dataRefreshPeriod
               WRITE setDataRefreshPeriod NOTIFY dataRefreshPeriodChanged)
    Q_PROPERTY(int urlTimeout READ urlTimeout WRITE setUrlTimeout NOTIFY urlTimeoutChanged)
    Q_PROPERTY(QVariantList urlMapList READ urlMapList WRITE setUrlMapList NOTIFY urlMapListChanged)
    Q_PROPERTY(int maxLogSize READ maxLogSize WRITE setMaxLogSize NOTIFY maxLogSizeChanged)
    Q_PROPERTY(int dataTTL READ dataTTL WRITE setDataTTL NOTIFY dataTTLChanged)

public:
    explicit Settings(QObject *parent = 0);
    QString version() const
    {
        return m_version;
    }
    QString panelId() const
    {
        return m_panelId;
    }
    int dataRefreshPeriod() const
    {
        return m_dataRefreshPeriod;
    }
    int urlTimeout() const
    {
        return m_urlTimeout;
    }
    QVariantList urlMapList() const
    {
        return m_urlMapList;
    }

    int maxLogSize() const
    {
        return m_maxLogSize;
    }

    int dataTTL() const
    {
        return m_dataTTL;
    }

signals:
    void error(QString errorString);
    void changed();
    void versionChanged(QString arg);
    void panelIdChanged(QString arg);
    void dataRefreshPeriodChanged(int arg);
    void urlTimeoutChanged(int arg);
    void validateJson(QByteArray data);
    void urlMapListChanged(QVariantList arg);

    void maxLogSizeChanged(int arg);

    void dataTTLChanged(int arg);

public slots:
    void readAll();
    void onError(QString errorString);
    void validate(QVariantMap map);
    bool setVersion(QString arg)
    {
        if (m_version != arg || m_version.isEmpty()) {
            if(arg.isEmpty())
            {
                emit error(QString("Settings: version is empty"));
                return false;
            }
            m_version = arg;
            emit versionChanged(arg);
        }
        return true;
    }
    bool setPanelId(QString arg)
    {
        if (m_panelId != arg || m_panelId.isEmpty()) {
            if(arg.isEmpty())
            {
                emit error(QString("Settings: panelId is empty"));
                return false;
            }
            m_panelId = arg;
            emit panelIdChanged(arg);
        }
        return true;
    }
    bool setDataRefreshPeriod(int arg)
    {
        if (m_dataRefreshPeriod != arg || m_dataRefreshPeriod < 1) {
            if(arg < 1)
            {
                emit error(QString("Settings: dataRefreshPeriod = %1").arg(arg));
                return false;
            }
            m_dataRefreshPeriod = arg * 1000;
            emit dataRefreshPeriodChanged(arg);
        }
        return true;
    }
    bool setUrlTimeout(int arg)
    {
        if (m_urlTimeout != arg || m_urlTimeout < 1) {
            if(arg < 1)
            {
                emit error(QString("Settings: urlTimeout = %1").arg(arg));
                return false;
            }
            m_urlTimeout = arg * 1000;
            emit urlTimeoutChanged(arg);
        }
        return true;
    }
    bool setUrlMapList(QVariantList arg)
    {
        if (m_urlMapList != arg || m_urlMapList.isEmpty()) {
            if(arg.isEmpty())
            {
                emit error("Settings: URL list is empty");
                return false;
            } else {
                m_urlMapList = arg;
                emit urlMapListChanged(arg);
            }
        }
        return true;
    }

    bool setMaxLogSize(int arg)
    {
        if (m_maxLogSize != arg) {
            if(arg < 0)
            {
                emit error(QString("Settings: maxLogSize = %1").arg(arg));
            } else {
                m_maxLogSize = arg;
                emit maxLogSizeChanged(arg);
            }
        }
        return true;
    }

    bool setDataTTL(int arg)
    {
        if (m_dataTTL != arg) {
            if(arg < 0)
            {
                emit error(QString("Settings: dataTTL = %1").arg(arg));
                return false;
            } else {
                m_dataTTL = arg;
                emit dataTTLChanged(arg);
            }
        }
        return true;
    }

private:
    QString m_version;
    QString m_panelId;
    int m_dataRefreshPeriod;
    int m_urlTimeout;
    QByteArray data;
    QVariantList m_urlMapList;
    int m_maxLogSize;
    int m_dataTTL;
};

#endif // SETTINGS_H
