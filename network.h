#ifndef NETWORK_H
#define NETWORK_H

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QStringList>
#include <QUrl>
#include <QFile>
#include <QTimer>
#include <QDebug>
#include <QThread>

#include "settings.h"

class Network : public QObject
{
    Q_OBJECT
public:
    explicit Network(QObject *parent = 0);
    ~Network();
    QVariantList urlMapList() const;
    void setUrlMapList(const QVariantList urlMapList);
    void appendUrlResult(QString result);
    QStringList urlResult() {return m_urlResult;}
signals:
    void done(QByteArray data, QString server);
    void error(QString errorString);
    void closeSession();

public slots:
    void onError(QString errorString);
    void exec();
    void onTimeout();
    void onFinished(QNetworkReply *);
    void onReplyFinished();
    void onReplyError(QNetworkReply::NetworkError code);
    int urlsLeft(){return m_urlsLeft;}
    void onNetworkAccessibleChanged(QNetworkAccessManager::NetworkAccessibility);

private:
    QVariantList m_urlMapList;
    QStringList m_urlResult;
    int m_urlsLeft;
    QByteArray m_data;
    QString m_alias;
    QString alias(QUrl url);
};

#endif // NETWORK_H
